# Documentation Rzine

**Document d'aide au processus de relecture d'une fiche Rzine :** [**consulter**](https://rzine.gitpages.huma-num.fr/help_rzine_reviewing)

[![licensebuttons by-sa](https://licensebuttons.net/l/by-sa/3.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0)
